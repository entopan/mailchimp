<?php namespace Entopancore\MailChimp;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'MailChimp',
            'description' => 'Provides MailChimp integration services.',
            'author'      => 'Alexey Bobkov, Samuel Georges'
        ];
    }


}
