<?php


Route::group(
    [
        'prefix' => 'api/v1/public/mailchimp',
        'middleware' => [
           'api'
        ],
    ], function () {
    Route::post('subscribe', ['uses' => 'Entopancore\MailChimp\Http\Controllers\MailChimpController@subscribe']);
});