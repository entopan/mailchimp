<?php namespace Entopancore\MailChimp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use ValidationException;
use ApplicationException;
use Newsletter;

class MailChimpController extends Controller
{
    public $request;
    public $key;
    public $class;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function subscribe()
    {
        $data = $this->request->input();
        $rules = [
            'email' => 'required|email|min:2|max:64',
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return getValidationResult($validation);
        }

        $options = [];
        if (isset($data["welcome"])) {
            $options["welcome"] = true;
        }
        try {
            $result = Newsletter::subscribe($data["email"], [], $data['list'], $options);
            return getSuccessResult($result);
        } catch (\Exception $e) {
            info($e->getMessage());
            return getErrorResult($e->getMessage());
        }
    }


}